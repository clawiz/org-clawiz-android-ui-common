/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.ui.common.generator;

import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.session.Session;

public abstract class AbstractAndroidGenerateContext {

    String packageName;
    String studioProjectPath;

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getStudioProjectPath() {
        return studioProjectPath;
    }

    public void setStudioProjectPath(String studioProjectPath) {
        this.studioProjectPath = studioProjectPath;
    }

    abstract public void prepare(Session session) throws CoreException;

}
