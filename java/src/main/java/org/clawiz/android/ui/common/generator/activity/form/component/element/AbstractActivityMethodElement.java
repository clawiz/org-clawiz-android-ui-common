/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.ui.common.generator.activity.form.component.element;

import org.clawiz.android.ui.common.generator.activity.form.FormActivityGenerator;
import org.clawiz.android.ui.common.generator.activity.form.component.AbstractFormClassComponent;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.generator.java.abstractjavaclassgenerator.element.MethodElement;

public class AbstractActivityMethodElement extends MethodElement {

    public AbstractFormClassComponent getFormClassComponent() {
        return (AbstractFormClassComponent) getComponent();
    }

    public FormActivityGenerator getFormActivityGenerator() {
        return getFormClassComponent().getFormActivityGenerator();
    }

    protected void prepareServerRequestContext() {

        addText("ServerRequestContext requestContext = new ServerRequestContext();");

    }



}
