/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.ui.common.generator.application.component;

import org.clawiz.android.common.generator.component.java.AbstractAndroidJavaClassComponent;
import org.clawiz.android.ui.common.generator.application.ApplicationGenerator;
import org.clawiz.android.ui.common.generator.application.GenerateApplicationContext;
import org.clawiz.core.common.CoreException;

public class AbstractApplicationComponent extends AbstractAndroidJavaClassComponent {

    ApplicationGenerator applicationGenerator;
    GenerateApplicationContext applicationContext;

    public ApplicationGenerator getApplicationGenerator() {
        return applicationGenerator;
    }

    public GenerateApplicationContext getApplicationContext() {
        return applicationContext;
    }

    @Override
    public void process() {
        super.process();

        applicationGenerator = (ApplicationGenerator) getGenerator();
        applicationContext = applicationGenerator.getApplicationContext();

        setPackageName(getApplicationContext().getPackageName());

    }

}
