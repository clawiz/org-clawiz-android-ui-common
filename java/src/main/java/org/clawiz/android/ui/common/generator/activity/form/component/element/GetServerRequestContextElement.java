/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.ui.common.generator.activity.form.component.element;

import org.clawiz.android.ui.common.generator.activity.layout.component.element.input.AbstractInputLayoutElement;

public class GetServerRequestContextElement extends AbstractActivityMethodElement {

    public static final String GET_DEFAULT_SERVER_REQUEST_CONTEXT_METHOD_NAME = "getDefaultServerRequestContext";

    @Override
    public void process() {
        super.process();
        setName(GET_DEFAULT_SERVER_REQUEST_CONTEXT_METHOD_NAME);

        setType("ServerRequestContext");

        addText("ServerRequestContext context = new ServerRequestContext();");

        addText("context.setUrl(getServerUrl());");

        addText("String value;");
        for(AbstractInputLayoutElement input: getFormActivityGenerator().getLayoutXMLComponent().getElementsByClass(AbstractInputLayoutElement.class) ) {

            if ( input.getId() != null ) {
                if ( input.getFormField() == null ) {
                    throwException("Cannot generate server request context set value for R.id. : form field not defined for input element", new Object[]{input.getId()});
                }
                addText("value = ((EditText)findViewById(R.id." + input.getId() + ")).getText().toString();");
                addText("context.addParameter(\""+input.getFormField().getName()+"\", value);");
            }

        }


        addText("return context;");

    }
}
