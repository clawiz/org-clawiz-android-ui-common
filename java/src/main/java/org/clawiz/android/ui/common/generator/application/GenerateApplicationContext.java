/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.ui.common.generator.application;

import org.clawiz.android.common.generator.AbstractAndroidGenerateContext;
import org.clawiz.android.common.generator.AndroidGenerateContext;
import org.clawiz.android.ui.common.generator.activity.GenerateActivityContext;
import org.clawiz.android.ui.common.generator.activity.form.GenerateFormContext;
import org.clawiz.android.ui.common.generator.menu.GenerateMenuContext;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.ui.common.data.application.Application;
import org.clawiz.ui.common.data.menu.Menu;
import org.clawiz.ui.common.data.view.form.Form;
import org.clawiz.ui.common.metadata.UiMetadataBase;

import java.util.ArrayList;
import java.util.HashMap;

public class GenerateApplicationContext extends AbstractAndroidGenerateContext {


    String      applicationName;
    Application application;

    ArrayList<GenerateActivityContext> activityContexts = new ArrayList<>();
    ArrayList<GenerateMenuContext>     menuContexts     = new ArrayList<>();

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public ArrayList<GenerateActivityContext> getActivityContexts() {
        return activityContexts;
    }

    private void fillContextValues(AndroidGenerateContext context) {
        context.setPackageName(getPackageName());
        context.setStudioProjectPath(getStudioProjectPath());
    }

    private  HashMap<Class, Class> nodeToContextMap = new HashMap<>();

    public <N extends MetadataNode, C extends AndroidGenerateContext> void addNodeToContextMap(Class<N> nodeClass, Class<C> contextClass) {
        nodeToContextMap.put(nodeClass, contextClass);
    }

    protected void prepareNodeToContextMap() {

        addNodeToContextMap(Menu.class, GenerateMenuContext.class);

        addNodeToContextMap(Form.class, GenerateFormContext.class);
    }

    public <C extends AbstractAndroidGenerateContext> AndroidGenerateContext newNodeContext(MetadataNode node) {
        Class<C> contextClass = nodeToContextMap.get(node.getClass());
        if ( contextClass == null ) {
            throw new CoreException("MetadataNode to GenerateContext map not contain value for node class '?'", node.getClass().getName());
        }
        AndroidGenerateContext context = null;
        try {
            context = contextClass.newInstance();
        } catch (Exception e) {
            throw new CoreException("Exception on create GenerateContext MetadataNode '?' of class '?' : ?", node.getName(), node.getClass().getName(), e.getMessage(), e);
        }
        context.setMetadataNode(node);
        fillContextValues(context);
        return context;
    }

    @Override
    public void prepare(Session session) {
        prepareNodeToContextMap();

        if ( application == null ) {
            application = session.getService(UiMetadataBase.class).getApplication(getPackageName(), getApplicationName(), false);
        }
        if ( application == null ) {
            throw new CoreException("Unknown application '?'", new Object[]{getPackageName() + "." + getApplicationName()});
        }
        applicationName = application.getName();

        for(Form form : application.getForms()) {
            activityContexts.add((GenerateActivityContext) newNodeContext(form));
        }

        for(Menu menu : application.getMenus()) {
            menuContexts.add((GenerateMenuContext) newNodeContext(menu));
        }

    }
}
