/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.ui.common.generator.application.server.component;

import org.clawiz.android.ui.common.generator.application.server.component.element.GetPostDataStringElement;
import org.clawiz.android.ui.common.generator.application.server.component.element.RequestServerElement;
import org.clawiz.core.common.CoreException;

public class ServerPrototypeComponent extends AbstractServerComponent {

    @Override
    public void process() {
        super.process();

        setName("ServerPrototype");

        addImport("android.app.Activity");
        addImport(getApplicationGenerator().getPackageName() + ".R");

        addImport("java.io.BufferedReader");
        addImport("java.io.BufferedWriter");
        addImport("java.io.IOException");
        addImport("java.io.InputStreamReader");
        addImport("java.io.OutputStream");
        addImport("java.io.OutputStreamWriter");
        addImport("java.io.UnsupportedEncodingException");
        addImport("java.net.HttpURLConnection");
        addImport("java.net.URL");
        addImport("java.net.URLEncoder");
        addImport("java.util.HashMap");
        addImport("java.util.Map");

        addImport("javax.net.ssl.HttpsURLConnection");

//        setExtends("Activity");

        addElement(GetPostDataStringElement.class);
        addElement(RequestServerElement.class);


    }

}
