/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.ui.common.generator.activity.form.component.element;

import org.clawiz.android.ui.common.generator.activity.layout.component.element.button.AbstractButtonElement;
import org.clawiz.core.common.CoreException;
import org.clawiz.ui.common.generator.view.form.servlet.component.element.method.FormApiServletCreateRequestMethodElement;

public class OnClickButtonElement extends AbstractActivityMethodElement {

    AbstractButtonElement buttonElement;

    public AbstractButtonElement getButtonElement() {
        return buttonElement;
    }

    public void setButtonElement(AbstractButtonElement buttonElement) {
        this.buttonElement = buttonElement;
    }

    @Override
    public void process() {
        super.process();
        setName(buttonElement.getOnClickMethodName());

        addParameter("android.view.View" ,"view");


        addText("AsyncTask requestTask = new AsyncTask() {");
        addText("    @Override");
        addText("    protected Object doInBackground(Object[] params) {");
        addText("        ServerRequestContext context = " + GetServerRequestContextElement.GET_DEFAULT_SERVER_REQUEST_CONTEXT_METHOD_NAME + "();");
        addText("        context.setPath(\"" + getFormActivityGenerator().getFormApiServletPath() +"\");");
        addText("        context.addParameter(\"" + FormApiServletCreateRequestMethodElement.ACTION_PARAMETER_NAME + "\", \"" + buttonElement.getButton().getName() +"\");");
        addText("        Server server = new Server();");
        addText("        server.requestServer(context);");
        addText("        processOnClickLoginRequestResult(context);");
        addText("        return null;");
        addText("    }");
        addText("};");
        addText("requestTask.execute();");
    }
}
