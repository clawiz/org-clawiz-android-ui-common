/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.ui.common.generator.menu;

import org.clawiz.android.common.generator.AbstractAndroidGenerateContext;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.ui.common.data.menu.Menu;
import org.clawiz.ui.common.metadata.UiMetadataBase;

public class GenerateMenuContext extends AbstractAndroidGenerateContext {

    Menu menu;
    String menuName;

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    @Override
    public void prepare(Session session) {
        if ( menu == null ) {
            menu = session.getService(UiMetadataBase.class).getMenu(getPackageName(), getMenuName());
        }
        if ( menu == null ) {
            throw new CoreException("Unknown UI menu '?'", new Object[]{getPackageName() + "." + getMenuName()});
        }
        menuName = menu.getName();
    }
}
