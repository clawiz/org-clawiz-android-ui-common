/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.ui.common.generator.activity.layout.component.element.button;

import org.clawiz.android.ui.common.generator.activity.layout.component.element.AbstractLayoutXMLElement;
import org.clawiz.core.xdk.utils.StringUtils;
import org.clawiz.ui.common.data.layout.LayoutComponent;
import org.clawiz.ui.common.data.layout.button.Button;

public class AbstractButtonElement extends AbstractLayoutXMLElement {

    @Override
    public String getTagName() {
        return "Button";
    }

    Button button;

    public Button getButton() {
        return button;
    }

    public void setButton(Button button) {
        this.button = button;
    }

    @Override
    public void setLayoutComponent(LayoutComponent layoutComponent) {
        setButton((Button) layoutComponent);
    }

    public String getOnClickMethodName() {
        return "onClick" + StringUtils.toUpperFirstChar(getButton().getName());
    }

    public String getProcessOnClickRequestResultMethodName() {
        return "process" + StringUtils.toUpperFirstChar(getOnClickMethodName()) + "RequestResult";
    }

    @Override
    public void process() {
        super.process();
        if ( getButton() == null ) {
            throwException("Button not defined");
        }
        fillSizeFromContainerOrientation();

        setElementId(getButton().getName());

        addAttribute("android:text", getButton().getText());
        addAttribute("android:onClick", getOnClickMethodName());
    }

}
