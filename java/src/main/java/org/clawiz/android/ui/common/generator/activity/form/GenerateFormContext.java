/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.ui.common.generator.activity.form;

import org.clawiz.android.ui.common.generator.activity.GenerateActivityContext;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.metadata.node.MetadataNode;
import org.clawiz.core.common.system.session.Session;
import org.clawiz.ui.common.data.view.form.Form;
import org.clawiz.ui.common.metadata.UiMetadataBase;

public class GenerateFormContext extends GenerateActivityContext {

    String formName;
    Form  form;

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    @Override
    public void setMetadataNode(MetadataNode metadataNode) {
        super.setMetadataNode(metadataNode);
        setForm((Form) metadataNode);
    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }

    @Override
    public void prepare(Session session) {

        if ( form == null ) {
            form = session.getService(UiMetadataBase.class).getForm(getPackageName(), getFormName(), false);
        }
        if ( form == null ) {
            throw new CoreException("Unknown UI form '?'", new Object[]{getPackageName() + "." + getFormName()});
        }
        formName = form.getName();

    }
}
