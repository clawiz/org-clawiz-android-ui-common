/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.ui.common.generator.activity.layout.component;

import org.clawiz.android.ui.common.generator.activity.AbstractActivityGenerator;
import org.clawiz.android.ui.common.generator.activity.layout.component.element.AbstractLayoutXMLElement;
import org.clawiz.android.ui.common.generator.activity.layout.component.element.button.DefaultButton;
import org.clawiz.android.ui.common.generator.activity.layout.component.element.container.ColumnLinearLayout;
import org.clawiz.android.ui.common.generator.activity.layout.component.element.container.LinearLayout;
import org.clawiz.android.ui.common.generator.activity.layout.component.element.container.RowLinearLayout;
import org.clawiz.android.ui.common.generator.activity.layout.component.element.input.TextInputLayout;
import org.clawiz.core.common.system.generator.abstractgenerator.AbstractGenerator;
import org.clawiz.core.common.system.generator.xml.component.AbstractXMLComponent;
import org.clawiz.core.xdk.utils.StringUtils;
import org.clawiz.ui.common.data.layout.LayoutComponent;
import org.clawiz.ui.common.data.layout.button.Button;
import org.clawiz.ui.common.data.layout.container.Container;
import org.clawiz.ui.common.data.layout.container.Column;
import org.clawiz.ui.common.data.layout.container.Row;
import org.clawiz.ui.common.data.layout.input.TextInput;

import java.math.BigDecimal;
import java.util.HashMap;

public abstract class AbstractActivityLayoutXMLComponent extends AbstractXMLComponent {

    protected abstract Class getRootElementClass();

    protected AbstractActivityGenerator getActivityGenerator() {
        return (AbstractActivityGenerator) getGenerator();
    }

    AbstractLayoutXMLElement rootElement;

    private String layoutName;

    HashMap<Class, Class> layoutToElementMap = new HashMap<>();

    public <L extends LayoutComponent, E extends AbstractLayoutXMLElement> void addLayoutComponentToElementMap(Class<L> layoutComponentClass, Class<E> elementClass) {
        layoutToElementMap.put(layoutComponentClass, elementClass);
    }

    protected <E extends AbstractLayoutXMLElement> Class<E> toElementClass(LayoutComponent layoutComponent) {
        return layoutComponent != null ? layoutToElementMap.get(layoutComponent.getClass()) : null;
    }


    protected <E extends AbstractLayoutXMLElement> AbstractLayoutXMLElement addLayoutComponentElement(LayoutComponent layoutComponent, AbstractLayoutXMLElement parentElement) {
        Class<E> elementClass = toElementClass(layoutComponent);
        if ( elementClass == null ) {
            throwException("Layout component to layout xml element map not contain value for component class '?'", new Object[]{layoutComponent.getClass().getName()});
        }
        AbstractLayoutXMLElement element = parentElement.addElement(elementClass);
        element.setLayoutComponent(layoutComponent);

        if ( layoutComponent instanceof Container ) {
            for(LayoutComponent child : ((Container) layoutComponent).getComponents() ) {
                addLayoutComponentElement(child, element);
            }
        }

        return element;
    }

    HashMap<String, BigDecimal> elemntsIdCache = new HashMap<>();
    public String getUniqueElementId(String sourceId) {
        if ( sourceId == null ) {
            return null;
        }

        String fullId      = getLayoutName() + "_" + StringUtils.toLowerFirstChar(sourceId);
        String key         = fullId.toUpperCase();
        BigDecimal counter = elemntsIdCache.get(key);

        if ( counter == null ) {
            counter = new BigDecimal(1);
            elemntsIdCache.put(key, counter);
            return fullId;
        } else {
            elemntsIdCache.put(key, counter.add(new BigDecimal(1)));
            return fullId + counter.intValue();
        }
    }

    public AbstractLayoutXMLElement getRootElement() {
        return rootElement;
    }

    public String getLayoutName() {
        return layoutName;
    }

    public void setLayoutName(String layoutName) {
        this.layoutName = layoutName;
    }

    @Override
    public void setGenerator(AbstractGenerator generator) {
        super.setGenerator(generator);
        setTargetPath(getActivityGenerator().getResLayoutPath());
    }

    protected void prepareLayoutComponentToElementMap() {
        addLayoutComponentToElementMap(Button.class, DefaultButton.class);

        addLayoutComponentToElementMap(Column.class, ColumnLinearLayout.class);
        addLayoutComponentToElementMap(Row.class,    RowLinearLayout.class);

        addLayoutComponentToElementMap(TextInput.class, TextInputLayout.class);

        addLayoutComponentToElementMap(org.clawiz.ui.common.data.layout.button.DefaultButton.class, DefaultButton.class);

    }

    @Override
    public void prepare() {
        super.prepare();
        prepareLayoutComponentToElementMap();
    }

    @Override
    public void process() {
        super.process();
        rootElement = addElement((Class<AbstractLayoutXMLElement>) getRootElementClass());
        rootElement.addAttribute("xmlns:android", "http://schemas.android.com/apk/res/android");
        rootElement.addAttribute("xmlns:tools",   "http://schemas.android.com/tools");

        if ( rootElement instanceof LinearLayout) {
            ((LinearLayout) rootElement).setOrientation(LinearLayout.LayoutOrientation.Vertical);
        }

        rootElement.setLayoutHeight(AbstractLayoutXMLElement.SIZE_MATCH_PARENT);
        rootElement.setLayoutWidth(AbstractLayoutXMLElement.SIZE_MATCH_PARENT);

        rootElement.setPaddingTop("@dimen/activity_vertical_margin");
        rootElement.setPaddingBottom("@dimen/activity_vertical_margin");
        rootElement.setPaddingLeft("@dimen/activity_horizontal_margin");
        rootElement.setPaddingRight("@dimen/activity_horizontal_margin");
    }


}
