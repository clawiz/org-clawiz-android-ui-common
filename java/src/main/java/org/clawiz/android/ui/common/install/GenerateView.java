/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.ui.common.install;

import org.clawiz.android.ui.common.generator.activity.form.FormActivityGenerator;
import org.clawiz.android.ui.common.generator.activity.form.GenerateFormContext;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.installer.AbstractInstallerCommand;
import org.clawiz.core.common.utils.RandomGUID;

public class GenerateView extends AbstractGenerateCommand {

    @Override
    public void process() {

        GenerateFormContext formContext = new GenerateFormContext();

        formContext.setPackageName(getCurrentPackage());
        formContext.setStudioProjectPath(getStudioProjectPath());

        formContext.setFormName(getAttribute(ATTRIBUTE_NAME, true));

        formContext.prepare(getSession());

        FormActivityGenerator activityGenerator = getService(FormActivityGenerator.class, new RandomGUID().toString());
        activityGenerator.setContext(formContext);
        activityGenerator.run();

    }
}
