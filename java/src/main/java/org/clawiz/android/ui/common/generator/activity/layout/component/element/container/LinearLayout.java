/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.ui.common.generator.activity.layout.component.element.container;

import org.clawiz.core.common.CoreException;
import org.clawiz.ui.common.data.layout.LayoutComponent;

public class LinearLayout extends AbstractContainerElement {

    public enum LayoutOrientation {

        Horizontal, Vertical

    }

    @Override
    public void setLayoutComponent(LayoutComponent layoutComponent) {

    }

    LayoutOrientation orientation;

    @Override
    public String getTagName() {
        return "LinearLayout";
    }

    public LayoutOrientation getOrientation() {
        return orientation;
    }

    public void setOrientation(LayoutOrientation orientation) {
        this.orientation = orientation;
    }

    @Override
    public void process() {
        super.process();
        if ( getOrientation() != null ) {
            addAttribute(ATTRIBUTE_ORIENTATION, getOrientation().toString().toLowerCase());
        }
    }

}
