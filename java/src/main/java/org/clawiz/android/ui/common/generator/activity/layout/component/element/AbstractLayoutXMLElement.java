/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.ui.common.generator.activity.layout.component.element;

import org.clawiz.android.ui.common.generator.activity.layout.component.AbstractActivityLayoutXMLComponent;
import org.clawiz.android.ui.common.generator.activity.layout.component.element.container.LinearLayout;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.generator.abstractgenerator.element.AbstractElement;
import org.clawiz.core.common.system.generator.xml.element.AbstractXMLElement;
import org.clawiz.ui.common.data.layout.LayoutComponent;

public abstract class AbstractLayoutXMLElement extends AbstractXMLElement {

    public static final String ATTRIBUTE_ID              = "android:id";

    public static final String ATTRIBUTE_LAYOUT_WIDTH    = "android:layout_width";
    public static final String ATTRIBUTE_LAYOUT_HEIGHT   = "android:layout_height";
    public static final String ATTRIBUTE_PADDING_BOTTOM  = "android:paddingBottom";
    public static final String ATTRIBUTE_PADDING_LEFT    = "android:paddingLeft";
    public static final String ATTRIBUTE_PADDING_RIGHT   = "android:paddingRight";
    public static final String ATTRIBUTE_PADDING_TOP     = "android:paddingTop";

    public static final String ATTRIBUTE_ORIENTATION     = "android:orientation";

    public static final String SIZE_MATCH_PARENT         = "match_parent";
    public static final String SIZE_WRAP_CONTENT         = "wrap_content";

    String layoutWidth;
    String layoutHeight;

    String paddingTop;
    String paddingBottom;
    String paddingLeft;
    String paddingRight;

    String id;

    LayoutComponent layoutComponent;

    AbstractActivityLayoutXMLComponent layoutXMLComponent;

    public AbstractActivityLayoutXMLComponent getLayoutXMLComponent() {
        if ( layoutXMLComponent == null) {
            layoutXMLComponent = (AbstractActivityLayoutXMLComponent) getComponent();
        }
        return layoutXMLComponent;
    }

    public LayoutComponent getLayoutComponent() {
        return layoutComponent;
    }

    abstract public void setLayoutComponent(LayoutComponent layoutComponent) throws CoreException;

    public String getId() {
        return id;
    }

    public void setElementId(String id) {
        this.id = getLayoutXMLComponent().getUniqueElementId(id);
    }

    public String getLayoutWidth() {
        return layoutWidth;
    }

    public void setLayoutWidth(String layoutWidth) {
        this.layoutWidth = layoutWidth;
    }

    public String getLayoutHeight() {
        return layoutHeight;
    }

    public void setLayoutHeight(String layoutHeight) {
        this.layoutHeight = layoutHeight;
    }

    public String getPaddingTop() {
        return paddingTop;
    }

    public void setPaddingTop(String paddingTop) {
        this.paddingTop = paddingTop;
    }

    public String getPaddingBottom() {
        return paddingBottom;
    }

    public void setPaddingBottom(String paddingBottom) {
        this.paddingBottom = paddingBottom;
    }

    public String getPaddingLeft() {
        return paddingLeft;
    }

    public void setPaddingLeft(String paddingLeft) {
        this.paddingLeft = paddingLeft;
    }

    public String getPaddingRight() {
        return paddingRight;
    }

    public void setPaddingRight(String paddingRight) {
        this.paddingRight = paddingRight;
    }

    @Override
    public void process() {
        super.process();
        fillSizeFromContainerOrientation();
    }

    @Override
    public void done() {
        super.done();

        if ( getId() != null ) {
            addAttribute(ATTRIBUTE_ID, "@+id/" + getId());
        }

        addAttribute(ATTRIBUTE_LAYOUT_WIDTH, getLayoutWidth());
        addAttribute(ATTRIBUTE_LAYOUT_HEIGHT, getLayoutHeight());

        addAttribute(ATTRIBUTE_PADDING_LEFT, getPaddingLeft());
        addAttribute(ATTRIBUTE_PADDING_RIGHT, getPaddingRight());
        addAttribute(ATTRIBUTE_PADDING_TOP, getPaddingTop());
        addAttribute(ATTRIBUTE_PADDING_BOTTOM, getPaddingBottom());

    }

    public LinearLayout.LayoutOrientation _getContainerOrientation(AbstractElement container) {
        if ( container instanceof LinearLayout ) {
            return ((LinearLayout) container).getOrientation();
        }
        if ( container instanceof AbstractLayoutXMLElement) {
            return _getContainerOrientation(container.getParent());
        }
        return null;
    }

    public LinearLayout.LayoutOrientation getContainerOrientation() {
        return _getContainerOrientation(getParent());
    }

    protected void fillSizeFromContainerOrientation() {
        if ( getContainerOrientation() == LinearLayout.LayoutOrientation.Vertical ) {
            setLayoutHeight(SIZE_WRAP_CONTENT);
            setLayoutWidth(SIZE_MATCH_PARENT);
        } else if (getContainerOrientation() == LinearLayout.LayoutOrientation.Horizontal) {
            setLayoutHeight(SIZE_MATCH_PARENT);
            setLayoutWidth(SIZE_WRAP_CONTENT);
        }
    }

}
