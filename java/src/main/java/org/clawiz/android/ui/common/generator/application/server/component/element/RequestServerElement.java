/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.ui.common.generator.application.server.component.element;

import org.clawiz.core.common.CoreException;

public class RequestServerElement extends AbstractServerMethodElement {

    @Override
    public void process() {
        super.process();
        setName("requestServer");

        addParameter("ServerRequestContext", "context");

        addText("String response = \"\";\n" +
                "\n" +
                "        try {\n" +
                "            URL url         = new URL(context.getUrl() + context.getPath() );\n" +
                "\n" +
                "            HttpURLConnection conn = (HttpURLConnection) url.openConnection();\n" +
                "            conn.setReadTimeout(15000);\n" +
                "            conn.setConnectTimeout(15000);\n" +
                "            conn.setRequestMethod(\"POST\");\n" +
                "            conn.setDoInput(true);\n" +
                "            conn.setDoOutput(true);\n" +
                "\n" +
                "            OutputStream os = conn.getOutputStream();\n" +
                "            BufferedWriter writer = new BufferedWriter(\n" +
                "                    new OutputStreamWriter(os, \"UTF-8\"));\n" +
                "            writer.write(getPostDataString(context.getParameters()));\n" +
                "\n" +
                "            writer.flush();\n" +
                "            writer.close();\n" +
                "            os.close();\n" +
                "            int responseCode=conn.getResponseCode();\n" +
                "\n" +
                "            context.setResponseCode(responseCode);\n" +
                "\n" +
                "            if (responseCode == HttpsURLConnection.HTTP_OK) {\n" +
                "                String line;\n" +
                "                BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));\n" +
                "                while ((line=br.readLine()) != null) {\n" +
                "                    response+=line;\n" +
                "                }\n" +
                "            } else {\n" +
                "                response=\"\";\n" +
                "            }\n" +
                "            context.setResponse(response);\n" +
                "        } catch (IOException e) {\n" +
                "            e.printStackTrace();\n" +
                "        }");

    }
}
