/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.ui.common.generator.activity.form.component;

import org.clawiz.android.ui.common.generator.activity.form.component.element.*;
import org.clawiz.android.ui.common.generator.application.server.component.element.GetServerUrlElement;
import org.clawiz.android.ui.common.generator.activity.layout.component.element.button.AbstractButtonElement;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.generator.java.abstractjavaclassgenerator.element.MethodElement;

public class FormActivityClassPrototypeComponent extends AbstractFormClassComponent {

    protected OnCreateMethod onCreate;
    protected OnCreateLoaderMethod onCreateLoader;
    protected OnLoadFinishedMethod onLoadFinished;
    protected OnLoadResetMethod onLoaderReset;
    protected GetServerRequestContextElement getServerRequestContext;

    public OnCreateMethod getOnCreate() {
        return onCreate;
    }

    public OnCreateLoaderMethod getOnCreateLoader() {
        return onCreateLoader;
    }

    public OnLoadFinishedMethod getOnLoadFinished() {
        return onLoadFinished;
    }

    public OnLoadResetMethod getOnLoaderReset() {
        return onLoaderReset;
    }

    public GetServerRequestContextElement getGetServerRequestContext() {
        return getServerRequestContext;
    }

    @Override
    public void process() {
        super.process();
        setName(getFormActivityGenerator().getFormActivityClassComponent().getName() + "Prototype");
        setFileExistsMode(FileExistsMode.OVERWRITE);

        setExtends(getFormActivityGenerator().getPackageName() + ".activity.AbstractActivity");
        setImplements("LoaderCallbacks<Cursor>");

        addImport("android.os.AsyncTask");
        addImport("android.support.v7.app.AppCompatActivity");
        addImport("android.app.LoaderManager.LoaderCallbacks");
        addImport("android.content.Loader");
        addImport("android.database.Cursor");

        addImport("android.os.Bundle");

        addImport("android.widget.EditText");

        addImport(getFormActivityGenerator().getPackageName() + ".R");

        addImport(getFormActivityGenerator().getPackageName() + ".server.Server");
        addImport(getFormActivityGenerator().getPackageName() + ".server.ServerRequestContext");

        onCreate       = addElement(OnCreateMethod.class);
        onCreateLoader = addElement(OnCreateLoaderMethod.class);
        onLoadFinished = addElement(OnLoadFinishedMethod.class);
        onLoaderReset  = addElement(OnLoadResetMethod.class);

        addElement(GetServerUrlElement.class);

        getServerRequestContext = addElement(GetServerRequestContextElement.class);

        for(AbstractButtonElement buttonElement : getFormActivityGenerator().getLayoutXMLComponent().getElementsByClass(AbstractButtonElement.class)) {
            OnClickButtonElement method = addElement(OnClickButtonElement.class);
            method.setButtonElement(buttonElement);

            MethodElement prccess = addMethod(buttonElement.getProcessOnClickRequestResultMethodName());
            prccess.addParameter("ServerRequestContext", "context");

        }

    }



}
