/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.ui.common.generator.activity.form;

import org.clawiz.android.common.generator.AbstractAndroidGenerateContext;
import org.clawiz.android.ui.common.generator.activity.AbstractActivityGenerator;
import org.clawiz.android.ui.common.generator.activity.form.component.FormActivityClassComponent;
import org.clawiz.android.ui.common.generator.activity.form.component.FormModelClassComponent;
import org.clawiz.android.ui.common.generator.activity.form.component.FormModelClassPrototypeComponent;
import org.clawiz.android.ui.common.generator.activity.layout.component.FormLayoutXMLComponent;
import org.clawiz.android.ui.common.generator.activity.form.component.FormActivityClassPrototypeComponent;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.xdk.utils.StringUtils;
import org.clawiz.ui.common.data.view.form.Form;
import org.clawiz.ui.common.metadata.UiMetadataBase;

public class FormActivityGenerator extends AbstractActivityGenerator {

    UiMetadataBase uiMetadataBase;

    FormLayoutXMLComponent              layoutXMLComponent;
    FormActivityClassPrototypeComponent formActivityClassPrototypeComponent;
    FormActivityClassComponent          formActivityClassComponent;

    FormModelClassComponent             formModelClassComponent;
    FormModelClassPrototypeComponent    formModelClassPrototypeComponent;

    GenerateFormContext formContext;

    public GenerateFormContext getFormContext() {
        return formContext;
    }

    @Override
    public void setContext(AbstractAndroidGenerateContext context) {
        super.setContext(context);
        this.formContext = (GenerateFormContext) context;
    }

    public Form getForm() {
        return formContext.getForm();
    }

    public FormLayoutXMLComponent getLayoutXMLComponent() {
        return layoutXMLComponent;
    }

    public FormActivityClassPrototypeComponent getFormActivityClassPrototypeComponent() {
        return formActivityClassPrototypeComponent;
    }

    public FormActivityClassComponent getFormActivityClassComponent() {
        return formActivityClassComponent;
    }

    public FormModelClassComponent getFormModelClassComponent() {
        return formModelClassComponent;
    }

    public FormModelClassPrototypeComponent getFormModelClassPrototypeComponent() {
        return formModelClassPrototypeComponent;
    }


    public String getFormApiServletPath() {
        return "/api/" + StringUtils.toLowerFirstChar(getForm().getName());
    }


    @Override
    protected void process() {
        super.process();

        logDebug("Start generate android activity for form '" + getPackageName() + "." + getForm().getName() + "'");

        formModelClassComponent             = addComponent(FormModelClassComponent.class);
        formActivityClassComponent          = addComponent(FormActivityClassComponent.class);

        layoutXMLComponent                  = addComponent(FormLayoutXMLComponent.class);

        formModelClassPrototypeComponent    = addComponent(FormModelClassPrototypeComponent.class);
        formActivityClassPrototypeComponent = addComponent(FormActivityClassPrototypeComponent.class);

    }

    @Override
    protected void done() {
        super.done();

        logDebug("Done generate android activity for form '" + getPackageName() + "." +  getForm().getName() + "'");
    }
}
