/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.ui.common.generator.activity.form.component;

import org.clawiz.android.ui.common.generator.activity.component.AbstractActivityClassComponent;
import org.clawiz.android.ui.common.generator.activity.form.FormActivityGenerator;
import org.clawiz.core.common.system.generator.GeneratorUtils;
import org.clawiz.ui.common.data.view.form.Form;

public class AbstractFormClassComponent extends AbstractActivityClassComponent {

    public static final String FORMS_PACKAGE_NAME = "form";

    Form form;

    public void setForm(Form form) {
        this.form = form;
    }

    public Form getForm() {
        return form;
    }

    public FormActivityGenerator getFormActivityGenerator() {
        return (FormActivityGenerator) getGenerator();
    }

    @Override
    public void process() {
        super.process();
        setForm(getFormActivityGenerator().getForm());

        setPackageName(getFormActivityGenerator().getPackageName() + "." + FORMS_PACKAGE_NAME + "." + GeneratorUtils.toJavaClassName(form.getName()).toLowerCase());
    }

}
