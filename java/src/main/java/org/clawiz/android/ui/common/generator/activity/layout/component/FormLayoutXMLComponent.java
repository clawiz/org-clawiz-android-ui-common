/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.ui.common.generator.activity.layout.component;

import org.clawiz.android.ui.common.generator.activity.form.FormActivityGenerator;
import org.clawiz.android.ui.common.generator.activity.layout.component.element.AbstractLayoutXMLElement;
import org.clawiz.android.ui.common.generator.activity.layout.component.element.container.LinearLayout;
import org.clawiz.android.ui.common.generator.activity.layout.component.element.container.ScrollView;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.system.generator.abstractgenerator.AbstractGenerator;
import org.clawiz.ui.common.data.layout.LayoutComponent;
import org.clawiz.ui.common.data.view.form.Form;


public class FormLayoutXMLComponent extends AbstractActivityLayoutXMLComponent {

    public static final String ATTRIBUTE_TOOLS_CONTEXT = "tools:context";

    Form form;

    protected FormActivityGenerator getFormGenerator() {
        return (FormActivityGenerator) getGenerator();
    }

    @Override
    protected Class getRootElementClass() {
        return LinearLayout.class;
    }

    @Override
    public void setGenerator(AbstractGenerator generator) {
        super.setGenerator(generator);
        setForm(getFormGenerator().getForm());
        setLayoutName("activity_" + getForm().getName().toLowerCase());
        setFileName(getLayoutName() + ".xml");
    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }

    @Override
    public void process() {
        super.process();
        getRootElement().addAttribute(ATTRIBUTE_TOOLS_CONTEXT,
                getFormGenerator().getFormActivityClassComponent().getPackageName() + "." + getFormGenerator().getFormActivityClassComponent().getName()
        );

        ScrollView mainScroll = getRootElement().addElement(ScrollView.class);
        mainScroll.setElementId("main_scroll");

        if (((LinearLayout) getRootElement() ).getOrientation() == LinearLayout.LayoutOrientation.Vertical ) {
            mainScroll.setLayoutHeight(AbstractLayoutXMLElement.SIZE_WRAP_CONTENT);
            mainScroll.setLayoutWidth(AbstractLayoutXMLElement.SIZE_MATCH_PARENT);
        } else {
            mainScroll.setLayoutHeight(AbstractLayoutXMLElement.SIZE_MATCH_PARENT);
            mainScroll.setLayoutWidth(AbstractLayoutXMLElement.SIZE_WRAP_CONTENT);
        }


//        int counter = 1;
        for(LayoutComponent component : getForm().getComponents() ) {
//            component.setName(getForm().getName() + "_form" + (getForm().getComponents().size() > 1 ? counter : ""));
//            counter++;
            addLayoutComponentElement(component, mainScroll);
        }
    }

}
