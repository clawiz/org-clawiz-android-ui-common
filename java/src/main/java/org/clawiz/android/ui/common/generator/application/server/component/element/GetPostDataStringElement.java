/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.ui.common.generator.application.server.component.element;

import org.clawiz.core.common.CoreException;

public class GetPostDataStringElement extends AbstractServerMethodElement {

    @Override
    public void process() {
        super.process();

        setType("String");
        setName("getPostDataString");
        addParameter("HashMap<String, String>" ,"params");

        setThrows("UnsupportedEncodingException");

        addText("StringBuilder result = new StringBuilder();\n" +
                "        boolean first = true;\n" +
                "        for(Map.Entry<String, String> entry : params.entrySet()){\n" +
                "            if (first)\n" +
                "                first = false;\n" +
                "            else\n" +
                "                result.append(\"&\");\n" +
                "\n" +
                "            result.append(URLEncoder.encode(entry.getKey(), \"UTF-8\"));\n" +
                "            result.append(\"=\");\n" +
                "            result.append(URLEncoder.encode(entry.getValue(), \"UTF-8\"));\n" +
                "        }\n" +
                "\n" +
                "        return result.toString();");

    }
}
