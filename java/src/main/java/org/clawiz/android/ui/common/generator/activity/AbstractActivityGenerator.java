/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.ui.common.generator.activity;

import org.clawiz.android.common.generator.AndroidGenerator;
import org.clawiz.android.common.generator.component.xml.manifest.AndroidManifestComponent;
import org.clawiz.core.common.CoreException;

public class AbstractActivityGenerator extends AndroidGenerator {

    AndroidManifestComponent androidManifestComponent;

    public AndroidManifestComponent getAndroidManifestComponent() {
        return androidManifestComponent;
    }

    @Override
    protected void process() {
        super.process();
        androidManifestComponent = addComponent(AndroidManifestComponent.class);
    }
}
