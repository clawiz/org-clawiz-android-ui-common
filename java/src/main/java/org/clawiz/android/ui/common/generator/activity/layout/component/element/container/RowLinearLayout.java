/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.ui.common.generator.activity.layout.component.element.container;

import org.clawiz.android.ui.common.generator.activity.layout.component.element.AbstractLayoutXMLElement;
import org.clawiz.ui.common.data.layout.LayoutComponent;
import org.clawiz.ui.common.data.layout.container.Row;

public class RowLinearLayout extends LinearLayout {

    Row row;


    public Row getRow() {
        return row;
    }

    public void setRow(Row row) {
        this.row = row;
    }

    @Override
    public void setLayoutComponent(LayoutComponent layoutComponent) {
        setRow((Row) layoutComponent);
    }

    @Override
    public void prepare() {
        super.prepare();

        setOrientation(LinearLayout.LayoutOrientation.Horizontal);
        setLayoutHeight(AbstractLayoutXMLElement.SIZE_MATCH_PARENT);
        setLayoutWidth(AbstractLayoutXMLElement.SIZE_WRAP_CONTENT);

    }
}
