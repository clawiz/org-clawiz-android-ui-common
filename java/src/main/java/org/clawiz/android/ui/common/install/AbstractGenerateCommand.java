/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.ui.common.install;

import org.clawiz.core.common.system.installer.AbstractInstallerCommand;

public class AbstractGenerateCommand extends AbstractInstallerCommand {

    public static final String STUDIO_PROJECT_PATH = "studio-project-path";

    public static final String ATTRIBUTE_NAME = "name";

    protected String getStudioProjectPath() {
        String studioPath = getStringParameter(STUDIO_PROJECT_PATH);
        if ( studioPath == null ) {
            studioPath = getAttribute(STUDIO_PROJECT_PATH);
        }
        if ( studioPath == null ) {
            throwException("Attribute or parameter '" + STUDIO_PROJECT_PATH + "' not defined");
        }
        return studioPath;
    }

    @Override
    public void process() {

    }
}
