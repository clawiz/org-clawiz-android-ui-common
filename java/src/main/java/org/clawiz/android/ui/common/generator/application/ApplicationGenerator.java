/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.ui.common.generator.application;

import org.clawiz.android.common.generator.AbstractAndroidGenerateContext;
import org.clawiz.android.common.generator.AndroidGenerator;
import org.clawiz.android.ui.common.generator.activity.GenerateActivityContext;
import org.clawiz.android.ui.common.generator.activity.form.FormActivityGenerator;
import org.clawiz.android.ui.common.generator.activity.form.GenerateFormContext;
import org.clawiz.android.ui.common.generator.application.server.component.ServerComponent;
import org.clawiz.android.ui.common.generator.application.server.component.ServerPrototypeComponent;
import org.clawiz.android.ui.common.generator.application.server.component.ServerRequestContextComponent;
import org.clawiz.android.ui.common.generator.application.server.component.ServerRequestContextPrototypeComponent;
import org.clawiz.core.common.utils.RandomGUID;

public class ApplicationGenerator extends AndroidGenerator {


    GenerateApplicationContext             applicationContext;

    ServerPrototypeComponent               serverPrototypeComponent;
    ServerRequestContextPrototypeComponent serverRequestContextPrototypeComponent;

    ServerComponent                        serverComponent;
    ServerRequestContextComponent          serverRequestContextComponent;

    public GenerateApplicationContext getApplicationContext() {
        return applicationContext;
    }

    @Override
    public void setContext(AbstractAndroidGenerateContext context) {
        super.setContext(context);
        this.applicationContext = (GenerateApplicationContext) context;
    }

    protected void generateActivity(GenerateActivityContext activityContext) {
        if ( activityContext instanceof GenerateFormContext ) {
            FormActivityGenerator activityGenerator = getService(FormActivityGenerator.class, new RandomGUID().toString());
            activityGenerator.setContext((AbstractAndroidGenerateContext) activityContext);
            activityGenerator.run();
            activityGenerator.destroy();
        } else {
            throwException("Wrong generate activity context class '?'", new Object[]{activityContext.getClass().getName()});
        }
    }

    @Override
    protected void process() {
        super.process();

        logDebug("Start generate android application " + getApplicationContext().getApplicationName());

        serverPrototypeComponent               = addComponent(ServerPrototypeComponent.class);
        serverRequestContextPrototypeComponent = addComponent(ServerRequestContextPrototypeComponent.class);

        serverComponent               = addComponent(ServerComponent.class);
        serverRequestContextComponent = addComponent(ServerRequestContextComponent.class);

        for(GenerateActivityContext ac : applicationContext.getActivityContexts()) {
            generateActivity(ac);
        }


    }
}
