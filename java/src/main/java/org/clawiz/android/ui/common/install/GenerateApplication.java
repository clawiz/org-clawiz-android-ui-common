/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.ui.common.install;

import com.sun.org.apache.xerces.internal.dom.DeferredElementImpl;
import org.clawiz.android.ui.common.generator.activity.form.GenerateFormContext;
import org.clawiz.android.ui.common.generator.application.ApplicationGenerator;
import org.clawiz.android.ui.common.generator.application.GenerateApplicationContext;
import org.clawiz.core.common.CoreException;
import org.clawiz.core.common.utils.RandomGUID;
import org.clawiz.ui.common.metadata.UiMetadataBase;
import org.w3c.dom.Node;

public class GenerateApplication extends AbstractGenerateCommand {

    UiMetadataBase uiMetadataBase;

    @Override
    public void process() {

        GenerateApplicationContext applicationContext = new GenerateApplicationContext();

        applicationContext.setPackageName(getCurrentPackage());
        applicationContext.setStudioProjectPath(getStudioProjectPath());

        applicationContext.setApplicationName(getAttribute(ATTRIBUTE_NAME, true));

        applicationContext.prepare(getSession());

        ApplicationGenerator applicationGenerator = getService(ApplicationGenerator.class, new RandomGUID().toString());
        applicationGenerator.setContext(applicationContext);
        applicationGenerator.run();

    }
}
