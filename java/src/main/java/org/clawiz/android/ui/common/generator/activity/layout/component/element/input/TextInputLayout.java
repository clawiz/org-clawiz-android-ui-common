/*
 * Copyright (c) 2016. Clawiz  Inc
 *
 * Contact:  http://www.clawiz.com
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 *
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 *
 * If you are unsure which license is appropriate for your use, please contact the sales department
 *
 */

package org.clawiz.android.ui.common.generator.activity.layout.component.element.input;


import org.clawiz.core.common.CoreException;
import org.clawiz.ui.common.data.layout.LayoutComponent;
import org.clawiz.ui.common.data.layout.input.TextInput;
import org.clawiz.ui.common.data.view.form.field.FormField;

public class TextInputLayout extends AbstractInputLayoutElement {

    EditText editElement;

    TextInput textInput;

    public TextInput getTextInput() {
        return textInput;
    }

    public void setTextInput(TextInput textInput) {
        this.textInput = textInput;
    }

    @Override
    public void setLayoutComponent(LayoutComponent layoutComponent) {
        setTextInput((TextInput) layoutComponent);
    }

    @Override
    public FormField getFormField() {
        return getTextInput().getFormField();
    }

    @Override
    public String getTagName() {
        return "android.support.design.widget.TextInputLayout";
    }

    @Override
    public void process() {
        super.process();

        editElement = addElement(EditText.class);
        editElement.setLayoutComponent(textInput);
    }
}
